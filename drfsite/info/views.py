from django.shortcuts import render
from django.forms import model_to_dict

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Info
from .serializers import InfoSerializer


# Create your views here.
# class InfoAPIView(generics.ListAPIView):
#     queryset = Info.objects.all()
#     serializer_class = InfoSerializer


class InfoAPIList(generics.ListCreateAPIView):
    queryset = Info.objects.all()
    serializer_class = InfoSerializer


class InfoAPIView(APIView):
    def get(self, request):
        items = Info.objects.all()
        return Response({'posts': InfoSerializer(items, many=True).data})

    def post(self, request):
        serializer = InfoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'post': serializer.data})  # InfoSerializer(post_new).data

    def put(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if not pk:
            return Response({'error': 'Method Put is not allowed'})

        try:
            instance = Info.objects.get(pk=pk)
        except:
            return Response({'error': 'Object does not exists'})

        serializer = InfoSerializer(data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'post': serializer.data})

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        if not pk:
            return Response({"error": "Method DELETE not allowed"})

        info = Info.objects.get(pk=pk)
        info.delete()

        return Response({"post": "delete post " + str(pk)})
